        <!-- Script -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.2.js"><\/script>')</script>
        <?php
        if (isset($js_inc) && $js_inc != NULL) {
            foreach ($js_inc as $value) {
                echo '<script src="assets/js/' . $value . '.js"></script>';
            }
        }
        ?>
        <script src="assets/js/main.js"></script>
    </body>
</html>