<?php   
    $page_title = 'Include PHP';
    $page_id = 'include_php';
    include 'inc/header.php';
?>

<div class="container">
    <h1><strong>Include PHP</strong></h1>

    <ol>
        <li>
            <p>header.php</p>

            <pre>
                &lt;html&gt;
                    &lt;head&gt;
                        &lt;title&gt;<code>&lt;?php echo $page_title;?&gt;</code>&lt;/title&gt;

                        &lt;link rel="stylesheet" href="assets/css/style.css" /&gt;

                        &lt;?php
                        if (isset(<code>$css_inc</code>) &amp;&amp; <code>$css_inc</code> != NULL) {
                            foreach (<code>$css_inc</code> as $value) {
                                echo '<code>&lt;link rel="stylesheet" href="assets/css/' . $value . '.css"&gt;</code>';
                            }
                        }
                        ?&gt; 
                    &lt;/head&gt;

                    &lt;body <code>&lt;?php echo ($page_id) ? ' id="page_'. $page_id .'"' : ''; ?&gt;</code>&gt;
            </pre>

            <p>footer.php</p>

            <pre>
                        &lt;script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"&gt;&lt;/script&gt;

                        &lt;?php
                        if (isset(<code>$js_inc</code>) &amp;&amp; <code>$js_inc</code> != NULL) {
                            foreach (<code>$js_inc</code> as $value) {
                                echo '<code>&lt;script src="assets/js/' . $value . '.js"&gt;&lt;/script&gt;</code>';
                            }
                        }
                        ?&gt;
                        &lt;script src="assets/js/main.js"&gt;&lt;/script&gt;
                    &lt;/body&gt;
                &lt;/html&gt;
            </pre>
            
            <p>index.php</p>

            <pre>
                &lt;?php include 'inc/header.php'; ?&gt;

                Now

                &lt;?php   
                    <code>$page_title</code> = 'Home Page';
                    <code>$page_id</code> = 'home';
                    <code>$css_inc</code> = array('gradient', 'responsive');
                    include 'inc/header.php';
                    <code>$js_inc</code> = array('gradient')
                ?&gt;

                &lt;!-- Content Here --&gt;

                &lt;?php include 'inc/footer.php'; ?&gt;
            </pre>

            <p>Output index.html</p>

            <pre>
                &lt;title&gt;<code>Home Page</code>&lt;/title&gt;

                &lt;body id="page_<code>home</code>"&gt;

                &lt;link rel="stylesheet" href="assets/css/<code>gradient</code>.css"&gt;

                &lt;script src="assets/js/<code>gradient</code>.js"&gt;&lt;/script&gt;
            </pre>
        </li>
    </ol>

    <h1><strong>END</strong></h1>

    <p><?php include 'inc/back.php'; ?></p>
</div>

<?php include 'inc/footer.php'; ?>