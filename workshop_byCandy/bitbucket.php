<?php   
    $page_title = 'Source Tree Page';
    $page_id = 'source_tree';
    include 'inc/header.php';
?>

<div class="container">
    <h1><strong>How to use <a href="http://rancoud.com/sourcetree-git-use/" target="_blank">Source Tree</a> (Bitbucket)</strong></h1>

    <ol>
        <li>
            <code>
                Commit: validate files stored in the index and save it with a comment
            </code>

            <p>
                Commit all your changes &amp; Please give a proper comment / Message
            </p>

            <img src="assets/img/bit_1.jpg" alt="bitbucket">

            <p class="red">Remember un-checked the tick <strong>Push changes immediately to origin/master</strong> before you COMMIT</p>
        </li>

        <li>
            <code>
                Fetch: retrieve all commits from all branchs from server's repository
            </code>
        </li>

        <li>
            <code>
                Pull: retrieve distant commits from server's repository
            </code>

            <img src="assets/img/bit_2.jpg" alt="">

            <p class="red">Remember un-checked the tick <strong>commit merged changes immediately</strong> before you COMMIT</p>
        </li>

        <li>
            <code>
                Push: take all local commits and send it to server's repository
            </code>
        </li>
    </ol>

    <h1><strong>END</strong></h1>

    <p><?php include 'inc/back.php'; ?></p>
</div>

<?php include 'inc/footer.php'; ?>