<?php   
    $page_title = 'Home Page';
    $page_id = 'home';
    $css_inc = array('gradient');
    include 'inc/header.php';
    $js_inc = array('gradient')
?>

<div class="container">
    <h1><strong>Welcome</strong></h1>

    <ol>
       <li>
           <a href="vhost.php">Creating virtual hosts on Apache XAMPP</a>
       </li>

       <li>
           <a href="bitbucket.php">How to use Source Tree (Bitbucket)</a>
       </li>

       <li>
           <a href="scss.php">SCSS Syntax</a>
       </li>

        <li>
            <a href="font_face.php">Custom Font - @font-face</a>
        </li>

       <li>
           <a href="icon_font.php">Fontello - icon fonts generator</a>
       </li>

       <li>
           <a href="include_php.php">Include PHP</a>
       </li>

       <li>
         <a href="image_sprites.php">Image Sprites</a>
       </li>
    </ol>

    <div id="url_references">
        <h1>References</h1>

        <ol>
            <li>Must have reset / normalize .css</li>

            <li>start a templates
            <a href="http://www.initializr.com/" target="_blank">Html Boilerplate templates</a></li>

            <li>Inspect Element is your Best Friend - <a href="http://getfirebug.com/">FireBug for Firefox</a></li>

            <li>Browser Support - google chrome, safari, firefox, IE11 - IE9; IE8 below show popup brower not support</li>
        </ol>
    </div>
</div>

<?php include 'inc/footer.php'; ?>