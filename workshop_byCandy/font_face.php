<?php   
    $page_title = 'Custom Font - @font-face';
    $page_id = 'font_face';
    include 'inc/header.php';
?>

<div class="container">
    <h1><strong><a href="http://www.font-face.com/#green_content" target="_blank">Custom Font - @font-face</a></strong></h1>
    
    <ol>
        <li>
            <p>font.scss</p>

            <pre>
                @font-face {
                    font-family: 'proxima_novablack_italic';
                    src: url('../fonts/petronas_fonts/proxima_nova_black_it-webfont.eot');
                    src: url('../fonts/petronas_fonts/proxima_nova_black_it-webfont.eot?#iefix') format('embedded-opentype'),
                         url('../fonts/petronas_fonts/proxima_nova_black_it-webfont.woff2') format('woff2'),
                         url('../fonts/petronas_fonts/proxima_nova_black_it-webfont.woff') format('woff'),
                         url('../fonts/petronas_fonts/proxima_nova_black_it-webfont.ttf') format('truetype'),
                         url('../fonts/petronas_fonts/proxima_nova_black_it-webfont.svg#proxima_novablack_italic') format('svg');
                    font-weight: normal;
                    font-style: normal;
                }
            </pre>

            <p>variables.scss</p>

            <pre>
                $font_stack: 'proxima_novablack', Helvetica, sans-serif;
            </pre>

            <p>style.scss</p>

            <pre>
                @import "font.scss";

                p{
                    font-family: $font_stack;
                }
            </pre>
        </li>
    </ol>

    <div id="url_references">
        <h1>References URL</h1>

        <ol>
            <li>
                <a href="http://www.fontsquirrel.com/tools/webfont-generator" target="_blank">Font Squirrel - Webfont Generator</a>
            </li>

            <li>
                <a href="http://www.cssfontstack.com/" target="_blank">CSS Font Stack</a>
            </li>

            <li>
                <a href="https://www.google.com/fonts" target="_blank">Google Webfont</a>
            </li>
        </ol>
    </div>

    <h1><strong>END</strong></h1>

    <p><?php include 'inc/back.php'; ?></p>
</div>

<?php include 'inc/footer.php'; ?>