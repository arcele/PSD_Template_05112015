<?php   
    $page_title = 'Image Sprites';
    $page_id = 'image_sprites';
    include 'inc/header.php';
?>

<div class="container">
    <h1><a href="https://css-tricks.com/css-sprites/" target="_blank"><strong>Image Sprites</strong></a></h1>

    <ol>
        <li>
            <strong>combining multiple images into a single image</strong>
            
            <pre>
                &lt;img src="assets/img/sprite.png" alt=""&gt;

                <img src="assets/img/sprite.png" alt="">
            </pre>

            <p>index.html</p>

            <pre>
                &lt;div class="flags-canada"&gt;
                    &lt;span&gt;canada flag&lt;/span&gt;
                &lt;/div&gt;

                &lt;div class="flags-usa"&gt;
                    &lt;span&gt;usa flag&lt;/span&gt;
                &lt;/div&gt;

                &lt;div class="flags-mexico"&gt;
                    &lt;span&gt;mexico flag&lt;/span&gt;
                &lt;/div&gt;
            </pre>

            <p><code>Using <a href="http://www.spritecow.com/" target="_blank">Sprite Cow</a> with your Sprites</code></p>

            <p>style.css</p>

            <pre>
                .flags-canada, .flags-mexico, .flags-usa {
                  background-image: url('../img/sprite.png');
                  background-repeat: no-repeat;
                }

                .flags-canada span, .flags-mexico span, .flags-usa span{
                    display: none;
                }

                .flags-canada {
                  height: 128px;
                  background-position: -5px -5px;
                }

                .flags-usa {
                  height: 135px;
                  background-position: -5px -143px;
                }

                .flags-mexico {
                  height: 147px;
                  background-position: -5px -288px;
                }
            </pre>
            
            <h1>SCSS</h1>

            <p>variable.scss</p>

            <pre>
                <code>$images</code>: "assets/img/";
            </pre>

            <p>style.scss</p>

            <pre>
                .sprite {
                  background: url("<code>#{$images}</code>sprite.png") no-repeat;
                  display: block;

                  span{
                    display: none;
                  }
                }
            </pre>
        </li>
    </ol>

    <h1><strong>END</strong></h1>

    <p><?php include 'inc/back.php'; ?></p>
</div>

<?php include 'inc/footer.php'; ?>