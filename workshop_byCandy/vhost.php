<?php   
    $page_title = 'vhost Page';
    $page_id = 'vhost';
    include 'inc/header.php';
?>

<div class="container">
    <h1><strong>Creating virtual hosts on Apache XAMPP</strong></h1>

    <p>For use only in a local testing environment on Windows. Virtual Hosts hosting multiple web sites running on a single server.</p>

    <ol>
        <li>
            <a href="https://delanomaloney.com/2013/07/how-to-set-up-virtual-hosts-using-xampp/" target="_blank">How to create Virtual Host on XAMPP</a>
        </li>

        <li>
            First assume you're using Windows machine and have <a href="https://www.apachefriends.org/index.html" target="_blank">XAMPP</a> installed
        </li>

        <li>
            Navigate to <code>C:/xampp/apache/conf/extra</code> or wherever your XAMPP files are located
        </li>

        <li>
            Open the file named <code>httpd-vhosts.conf</code> with a text editor.
        </li>

        <li>
            Around line 19 / 20 find <code># NameVirtualHost *:80</code> and uncomment or remove the <code># hash</code>.
        </li>

        <li>
            At the very bottom of the file paste the following code:

            <pre>
                &lt;VirtualHost *:80&gt;
                DocumentRoot "C:/xampp/htdocs/"
                ServerName localhost
                ServerAlias localhost
                &lt;/VirtualHost&gt;
           </pre>

           <p>The above code is to view localhost</p>
        </li>

        <li>
            Now you can copy and paste the code above to add your Virtual Host directories. For example I'm working on a site called <code>Hello World</code>:

            <pre>
                &lt;VirtualHost *:80&gt;
                    DocumentRoot "C:/xampp/htdocs/<code>hello_world</code>"
                    ServerName <code>hello_world</code>.dev
                    ServerAlias www.<code>hello_world</code>.dev
                &lt;/VirtualHost&gt;
            </pre>
            
            <p>This code below is comment, easy for you to copy and paste to host file</p>
            <pre>
                ##127.0.0.1  <code>hello_world</code>.dev  www.<code>hello_world</code>.dev
            </pre>
        </li>

        <li>
            Next head over to your <a href="http://www.viper007bond.com/2011/08/13/editing-your-hosts-file-in-windows/" target="_blank">Windows host file</a> to edit your HOSTS. The file will be located at <code>C:/Windows/System32/drivers/etc/</code>
        </li>

        <li>
            Open the file named <code>hosts</code> with notepad <span class="red">open up as administrative privileges</span>

            <img src="assets/img/host_01.jpg" alt="host administrative">

            <img src="assets/img/host_02.jpg" alt="host notepad">
        </li>

        <li>
            Look for

            <pre>
                # localhost name resolution is handled within DNS itself.
                #   127.0.0.1       localhost
                #   ::1         localhost
            </pre>

            <p>Some of you need to uncomment or remove the <code># hash</code></p>

            <pre>
                # localhost name resolution is handled within DNS itself.
                   127.0.0.1       localhost
                   ::1         localhost
            </pre>

            <p>and add the following just after that line:</p>

            <pre>
                127.0.0.1  <code>hello_world</code>.dev  www.<code>hello_world</code>.dev
            </pre>
        </li>

        <li>
            Restart Apache
        </li>

        <li>
            Now Paste URL <strong>hello_world.dev</strong> to any browser to view your Site as Vhost 
        </li>
    </ol>

    <h1><strong>END</strong></h1>

    <p><?php include 'inc/back.php'; ?></p>
</div>

<?php include 'inc/footer.php'; ?>