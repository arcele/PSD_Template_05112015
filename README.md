# **HTML & CSS Slicing Tutorial Workshop**

I have created a "html template" for your references, you may check around.

Require Material Below

1.  Before you start, pick 1 of the PSD template and start slicing tutorial workshop

2.  Use only HTML & CSS code, save your index as PHP file. **Avoid using index.html** **You may view your PHP by Vhost**

    ## [how to create virtual host on XAMPP](https://delanomaloney.com/2013/07/how-to-set-up-virtual-hosts-using-xampp/)

    1.  You need to have [XAMPP installed](https://www.apachefriends.org/index.html)

    2.  Navigate to **C:\xampp\apache\conf\extra\** folder open up **httpd-vhosts.conf** text editor with administrative privileges / Open up in Sublime

    3.  Find **NameVirtualHost *:80** - Remove **##**

    4.  Copy and Paste the code below to the most bottom  
        This is to view localhost

        **<VirtualHost *:80>  
        DocumentRoot "C:/xampp/htdocs/"  
        ServerName localhost  
        ServerAlias localhost  
        </VirtualHost>**

        This is to view your PHP on Vhost  
        **<VirtualHost *:80>  
        DocumentRoot "C:/xampp/htdocs/folder_name"  
        ServerName folder_name.dev  
        ServerAlias www.folder_name.dev  
        </VirtualHost>**

        This code is comment for further use  
        **##127.0.0.1 folder_name.dev www.folder_name.dev**

    5.  Now we head over to our Windows Hosts File, to edit the HOSTS. the file will be located at **C:/Windows/System32/drivers/etc/hosts**, where hosts is the file. open up note pad with administrative privileges / Open up in Sublime

        Some of you need to un-comment **#**  
        127.0.0.1 localhost  
        ::1 localhost

        Paste this to the most bottom of the host file  
        **127.0.0.1 folder_name.dev www.folder_name.dev**

    6.  Paste URL **folder_name.dev** to any browser to view your PHP as Vhost

3.  **Must** Update your file into Bitbucket (Source Tree)
    1.  **Commit** all your changes & Please give a proper Message
        ![bit_1.jpg](https://bitbucket.org/repo/Gd6Rza/images/2328269736-bit_1.jpg)

    2.  **fetch** To grab the latest updates

    3.  **pull** To merge your file with other & Remeber always PULL before you continue your work
        ![bit_2.jpg](https://bitbucket.org/repo/Gd6Rza/images/3274555108-bit_2.jpg)

    4.  **push** Share your file with everyone


4.  [Learn to Code HTML & CSS](http://learn.shayhowe.com/)

5.  start a templates [Html Boilerplate templates](http://www.initializr.com/)

6.  **Must have** reset / normalize .css

7.  **Must have** Image Sprites css - [sprite cow](http://www.spritecow.com/)

8.  **Must have** [SCSS Syntax -](http://sass-lang.com/guide) **include mixins & variables**

9.  **Must have** CSS shorthand

10.  **Must have** [Custom Font - @font-face](http://www.font-face.com/#green_content)

11.  **Must have** [Fontello - icon fonts generator](http://fontello.com/) **avoid using images as icon** use [SVG convert as Icon](https://codyhouse.co/gem/animate-svg-icons-with-css-and-snap/)

12.  Browser Support - google chrome, safari, firefox, IE11-IE9 **IE8 below show popup brower not support**

13.  Inspect Element is your Best Friend - [FireBug for Firefox](http://getfirebug.com/)

14.  Avoid inline CSS

15.  Avoid using Bootstrap (only for this project)

16.  **No Responsive (only for this project)** if you have time you may Do Responsive




If you have question can always ask.

Once you done your slicing, we can discuss together what can be improved. Thanks.

PS: You can explore more if you have free time or share your knowledge with others.





# References URL

1.  [quick and simple image placeholder](https://placehold.it/)
2.  [Become a git guru.](https://www.atlassian.com/git/tutorials/)
3.  [SUBLIME TEXT 3 - TIPS](https://blog.generalassemb.ly/sublime-text-3-tips-tricks-shortcuts/)
4.  [CSS font stacks](http://www.cssfontstack.com/)